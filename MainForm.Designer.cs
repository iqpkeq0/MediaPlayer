﻿using System;

namespace MediaPlayer
{
    partial class f_MediaPlayer
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.b_open = new System.Windows.Forms.Button();
            this.b_previous = new System.Windows.Forms.Button();
            this.b_next = new System.Windows.Forms.Button();
            this.PlayListBox = new System.Windows.Forms.ListBox();
            this.b_play = new System.Windows.Forms.Button();
            this.b_stop = new System.Windows.Forms.Button();
            this.VolBar = new System.Windows.Forms.TrackBar();
            this.Vol = new System.Windows.Forms.Label();
            this.timeTrackBar = new System.Windows.Forms.TrackBar();
            this.currentTimeLabel = new System.Windows.Forms.Label();
            this.TotalTimeLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeSplitLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.rdPicBox = new System.Windows.Forms.PictureBox();
            this.controlPlaylistBox1 = new System.Windows.Forms.ListBox();
            this.b_playSchedule = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.VolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTrackBar)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // b_open
            // 
            this.b_open.Location = new System.Drawing.Point(13, 13);
            this.b_open.Name = "b_open";
            this.b_open.Size = new System.Drawing.Size(75, 23);
            this.b_open.TabIndex = 0;
            this.b_open.Text = "打开";
            this.b_open.UseVisualStyleBackColor = true;
            this.b_open.Click += new System.EventHandler(this.b_open_Click);
            // 
            // b_previous
            // 
            this.b_previous.Location = new System.Drawing.Point(99, 13);
            this.b_previous.Name = "b_previous";
            this.b_previous.Size = new System.Drawing.Size(75, 23);
            this.b_previous.TabIndex = 1;
            this.b_previous.Text = "上一曲";
            this.b_previous.UseVisualStyleBackColor = true;
            this.b_previous.Click += new System.EventHandler(this.b_previous_Click);
            // 
            // b_next
            // 
            this.b_next.Location = new System.Drawing.Point(180, 13);
            this.b_next.Name = "b_next";
            this.b_next.Size = new System.Drawing.Size(75, 23);
            this.b_next.TabIndex = 2;
            this.b_next.Text = "下一曲";
            this.b_next.UseVisualStyleBackColor = true;
            this.b_next.Click += new System.EventHandler(this.b_next_Click);
            // 
            // PlayListBox
            // 
            this.PlayListBox.FormattingEnabled = true;
            this.PlayListBox.ItemHeight = 12;
            this.PlayListBox.Location = new System.Drawing.Point(13, 42);
            this.PlayListBox.Name = "PlayListBox";
            this.PlayListBox.Size = new System.Drawing.Size(221, 208);
            this.PlayListBox.TabIndex = 3;
            this.PlayListBox.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // b_play
            // 
            this.b_play.Location = new System.Drawing.Point(271, 12);
            this.b_play.Name = "b_play";
            this.b_play.Size = new System.Drawing.Size(75, 23);
            this.b_play.TabIndex = 4;
            this.b_play.Text = "播放";
            this.b_play.UseVisualStyleBackColor = true;
            this.b_play.Click += new System.EventHandler(this.b_play_Click);
            // 
            // b_stop
            // 
            this.b_stop.Location = new System.Drawing.Point(352, 12);
            this.b_stop.Name = "b_stop";
            this.b_stop.Size = new System.Drawing.Size(75, 23);
            this.b_stop.TabIndex = 5;
            this.b_stop.Text = "停止";
            this.b_stop.UseVisualStyleBackColor = true;
            this.b_stop.Click += new System.EventHandler(this.b_stop_Click);
            // 
            // VolBar
            // 
            this.VolBar.AllowDrop = true;
            this.VolBar.LargeChange = 1;
            this.VolBar.Location = new System.Drawing.Point(522, 28);
            this.VolBar.Maximum = 100;
            this.VolBar.Name = "VolBar";
            this.VolBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.VolBar.Size = new System.Drawing.Size(45, 123);
            this.VolBar.TabIndex = 6;
            this.VolBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.VolBar.ValueChanged += new System.EventHandler(this.VolBar_ValueChanged);
            // 
            // Vol
            // 
            this.Vol.AutoSize = true;
            this.Vol.Location = new System.Drawing.Point(520, 13);
            this.Vol.Name = "Vol";
            this.Vol.Size = new System.Drawing.Size(29, 12);
            this.Vol.TabIndex = 7;
            this.Vol.Text = "音量";
            // 
            // timeTrackBar
            // 
            this.timeTrackBar.LargeChange = 1;
            this.timeTrackBar.Location = new System.Drawing.Point(172, 274);
            this.timeTrackBar.Margin = new System.Windows.Forms.Padding(0, 3, 3, 0);
            this.timeTrackBar.Maximum = 100;
            this.timeTrackBar.Minimum = 1;
            this.timeTrackBar.Name = "timeTrackBar";
            this.timeTrackBar.Size = new System.Drawing.Size(387, 45);
            this.timeTrackBar.SmallChange = 0;
            this.timeTrackBar.TabIndex = 8;
            this.timeTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.timeTrackBar.Value = 1;
            this.timeTrackBar.Scroll += new System.EventHandler(this.TimeTrackBar_Scroll);
            // 
            // currentTimeLabel
            // 
            this.currentTimeLabel.AutoSize = true;
            this.currentTimeLabel.Location = new System.Drawing.Point(18, 11);
            this.currentTimeLabel.Name = "currentTimeLabel";
            this.currentTimeLabel.Size = new System.Drawing.Size(35, 12);
            this.currentTimeLabel.TabIndex = 9;
            this.currentTimeLabel.Text = "00:00";
            // 
            // TotalTimeLabel
            // 
            this.TotalTimeLabel.AutoSize = true;
            this.TotalTimeLabel.Location = new System.Drawing.Point(61, 11);
            this.TotalTimeLabel.Name = "TotalTimeLabel";
            this.TotalTimeLabel.Size = new System.Drawing.Size(35, 12);
            this.TotalTimeLabel.TabIndex = 10;
            this.TotalTimeLabel.Text = "00:00";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.timeSplitLabel);
            this.panel1.Controls.Add(this.currentTimeLabel);
            this.panel1.Controls.Add(this.TotalTimeLabel);
            this.panel1.Location = new System.Drawing.Point(73, 266);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(101, 31);
            this.panel1.TabIndex = 11;
            // 
            // timeSplitLabel
            // 
            this.timeSplitLabel.AutoSize = true;
            this.timeSplitLabel.Location = new System.Drawing.Point(51, 11);
            this.timeSplitLabel.Name = "timeSplitLabel";
            this.timeSplitLabel.Size = new System.Drawing.Size(11, 12);
            this.timeSplitLabel.TabIndex = 11;
            this.timeSplitLabel.Text = "/";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(240, 42);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 208);
            this.textBox1.TabIndex = 12;
            // 
            // rdPicBox
            // 
            this.rdPicBox.Image = global::MediaPlayer.Properties.Resources.PlaySingle;
            this.rdPicBox.Location = new System.Drawing.Point(440, 2);
            this.rdPicBox.Margin = new System.Windows.Forms.Padding(1);
            this.rdPicBox.Name = "rdPicBox";
            this.rdPicBox.Size = new System.Drawing.Size(36, 36);
            this.rdPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rdPicBox.TabIndex = 13;
            this.rdPicBox.TabStop = false;
            this.rdPicBox.Click += new System.EventHandler(this.rdPicBox_Click);
            // 
            // controlPlaylistBox1
            // 
            this.controlPlaylistBox1.FormattingEnabled = true;
            this.controlPlaylistBox1.ItemHeight = 12;
            this.controlPlaylistBox1.Items.AddRange(new object[] {
            "单曲播放",
            "单曲循环",
            "顺序播放",
            "列表循环",
            "随机播放"});
            this.controlPlaylistBox1.Location = new System.Drawing.Point(440, 37);
            this.controlPlaylistBox1.Name = "controlPlaylistBox1";
            this.controlPlaylistBox1.Size = new System.Drawing.Size(63, 64);
            this.controlPlaylistBox1.TabIndex = 14;
            this.controlPlaylistBox1.Visible = false;
            this.controlPlaylistBox1.Click += new System.EventHandler(this.controlPlaylistBox1_Click);
            this.controlPlaylistBox1.MouseLeave += new System.EventHandler(this.controlPlaylistBox1_MouseLeave);
            // 
            // b_playSchedule
            // 
            this.b_playSchedule.Location = new System.Drawing.Point(484, 190);
            this.b_playSchedule.Name = "b_playSchedule";
            this.b_playSchedule.Size = new System.Drawing.Size(75, 23);
            this.b_playSchedule.TabIndex = 15;
            this.b_playSchedule.Text = "播放计划";
            this.b_playSchedule.UseVisualStyleBackColor = true;
            this.b_playSchedule.Click += new System.EventHandler(this.b_playSchedule_Click);
            // 
            // f_MediaPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 305);
            this.Controls.Add(this.b_playSchedule);
            this.Controls.Add(this.controlPlaylistBox1);
            this.Controls.Add(this.rdPicBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.timeTrackBar);
            this.Controls.Add(this.Vol);
            this.Controls.Add(this.VolBar);
            this.Controls.Add(this.b_stop);
            this.Controls.Add(this.b_play);
            this.Controls.Add(this.PlayListBox);
            this.Controls.Add(this.b_next);
            this.Controls.Add(this.b_previous);
            this.Controls.Add(this.b_open);
            this.Name = "f_MediaPlayer";
            this.Text = "播放器";
            ((System.ComponentModel.ISupportInitialize)(this.VolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTrackBar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdPicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        







        #endregion

        private System.Windows.Forms.Button b_open;
        private System.Windows.Forms.Button b_previous;
        private System.Windows.Forms.Button b_next;
        private System.Windows.Forms.ListBox PlayListBox;
        private System.Windows.Forms.Button b_play;
        private System.Windows.Forms.Button b_stop;
        private System.Windows.Forms.TrackBar VolBar;
        private System.Windows.Forms.Label Vol;
        private System.Windows.Forms.TrackBar timeTrackBar;
        private System.Windows.Forms.Label currentTimeLabel;
        private System.Windows.Forms.Label TotalTimeLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label timeSplitLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox rdPicBox;
        private System.Windows.Forms.ListBox controlPlaylistBox1;
        private System.Windows.Forms.Button b_playSchedule;
    }
}

