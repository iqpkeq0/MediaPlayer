﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace MediaPlayer
{
    class SqlLiteHelper
    {
        #region 连接字符串
        /// <summary>
        /// 连接数据库字符串
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            string url= "Data Source=" + System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "test.db";
            return url;

        }
        #endregion

        /// <summary>
        /// 插入任务
        /// </summary>
        /// <param name="eventName">任务计划名称</param>
        /// <param name="duration">周期cron表达式</param>
        public static void InsertEvent(String eventName = "", String duration = "")
        {
            SQLiteConnection connection = new SQLiteConnection(SqlLiteHelper.GetConnectionString());
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = "insert into eventList(eventName,duration) values(@eventName,@duration)"
            };//新增一个command
            SQL.Parameters.AddWithValue("@eventName", eventName);
            SQL.Parameters.AddWithValue("@duration", duration);
            using (connection)
            {
                connection.Open();
                SQL.ExecuteNonQuery();
            }
        }

        #region 动作组 系列 Action SQL
        /// <summary>
        /// SQL连接
        /// </summary>
        private static SQLiteConnection connection = null;
        /// <summary>
        /// 插入动作组
        /// </summary>
        /// <param name="actionName">动作组名</param>
        public static void InsertAction(String actionName = "")
        {
            Init();
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = "insert into actionList(actionName) values(@actionName);"
            };//新增一个command
            SQL.Parameters.AddWithValue("@actionName", actionName);
            using (connection)
            {
                SQL.ExecuteNonQuery();
                Dispose();
            }
        }

        /// <summary>
        /// 更新动作组
        /// </summary>
        /// <param name="NewActionName">新动作组名</param>
        /// <param name="OldActionName">已有动作组名</param>

        public static void UpdateAction(String NewActionName ="", String OldActionName = "")
        {
            Init();
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = "update actionList set actionName=@Parameter1 where actionName=@Parameter2;"
                 //CommandText = "update actionList set actionName=\""+NewActionName+"\" where actionName=\""+ OldActionName + "\";"
            };
            SQL.Parameters.AddWithValue("@Parameter1", NewActionName);//参数替换
            SQL.Parameters.AddWithValue("@Parameter2", OldActionName);
            using (connection)
            {
                object effectRows = SQL.ExecuteScalar();
                Dispose();
            }
        }
        /// <summary>
        /// 查询动作组  返回动作组 list <string>123</string>
        /// </summary>
        /// <returns>返回动作组的list</returns>
        public static List<string> SelectAction()
        {
            List<string> result = new List<string> { };
            Init();
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = "select actionName  from actionList;"
            };

            
            using (connection)
            {

                SQLiteDataReader sQLiteData = SQL.ExecuteReader();
             //  sQLiteData.Read();                
                for (int i = 0; i < sQLiteData.FieldCount; i++)
                {
                    while (sQLiteData.Read()) { 
                        object o = sQLiteData.GetValue(i);
                        result.Add(o.ToString());
                    }
                }
                //SQL.ExecuteNonQuery();只执行没结果
                Dispose();
            }
            return result;

        }
        /// <summary>
        /// 删除动作组名
        /// </summary>
        /// <param name="actionName"></param>
        public static void DeleteAction(string actionName="")
        {
            Init();
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = "delete from actionList where actionName= @actionName"
            };
            SQL.Parameters.AddWithValue("@actionName", actionName);
            using (connection)
            {
                SQL.ExecuteNonQuery();
                Dispose();
            }
        }

        #endregion

        #region SQL 参数化 测试代码
        /// <summary>
        /// SQL执行
        /// </summary>
        /// <param name="does"></param>
        /// <param name="dBname"></param>
        /// <param name="args">数组前面是字段值，后面是where condition的值</param>
        public static void ExecuteSQL(DOING does, DBname dBname,string[] args)
        {
            SetPreEndCommand(does);//设定前缀SQL，后缀SQL;
            SetDBName(dBname);//设定表名
            SetColumnName(dBname);//设置字段名
            string sql = null;
            //TODO:Testing----------------SQL参数化
            switch (does)
            {
                case DOING.SELECT:
                    sql = PreCommand + TableName + EndCommand;
                    break;
                case DOING.INSERT:
                    if (dBname == DBname.ACTION)
                    {
                        sql=PreCommand+TableName+"("+ ColumnName[1] + ")"+" values("+args+") "+EndCommand;
                    }
                    else if (dBname == DBname.CHILDACTION)
                    {
                        sql = PreCommand + TableName + "(" + ColumnName[1] + ColumnName[2] + ColumnName[3] + ")" + " values(" + args + ") " + EndCommand;
                    }
                    else if (dBname == DBname.EVENT)
                    {
                        sql = PreCommand + TableName + "(" + ColumnName[1] + ColumnName[2] + ColumnName[3] + ")" + " values(" + args + ") " + EndCommand;
                    }
                    break;
                case DOING.UPDATE:
                    if (dBname == DBname.ACTION)
                    {
                        sql = PreCommand + TableName +" set "+ ColumnName[1] +"='"+args[0]+"'"+ EndCommand + ColumnName[0]+"="+args[1];
                    }
                    else if (dBname == DBname.CHILDACTION)
                    {

                    }
                    else if (dBname == DBname.EVENT)
                    {

                    }
                    break;
                case DOING.DELETE:
                    if (dBname == DBname.ACTION)
                    {

                    }
                    else if (dBname == DBname.CHILDACTION)
                    {

                    }
                    else if (dBname == DBname.EVENT)
                    {

                    }
                    break;
                default:
                    break;
            }



            Init();
            SQLiteCommand SQL = new SQLiteCommand(connection)
            {
                CommandText = PreCommand + TableName+" (actionName) values(@actionName)" +EndCommand
            };//新增一个command
            //SQL.Parameters.AddWithValue("@actionName", actionName); 替换command中的参数
            using (connection)
            {
                SQL.ExecuteNonQuery();
                Dispose();
            }
        }

        private static string _commandPre = null;
        private static string _commandEnd = null;
        /// <summary>
        /// 前置命令
        /// </summary>
        public static string PreCommand
        {
            get { return _commandPre; }
            set { _commandPre = value; }
        }
        /// <summary>
        /// 后置命令
        /// </summary>
        public static string EndCommand
        {
            get { return _commandEnd; }
            set { _commandEnd = value; }
        }
        /// <summary>
        /// 枚举表
        /// </summary>
        public enum DBname
        {
            /// <summary>
            /// 计划
            /// </summary>
            EVENT,
            /// <summary>
            /// 动作组
            /// </summary>
            ACTION,
            /// <summary>
            /// 执行动作
            /// </summary>
            CHILDACTION
        }

        private static string _tableName = null;
        /// <summary>
        /// 表名
        /// </summary>
        public static string TableName{
            get { return _tableName; }
            set { _tableName = value; }
         }

        /// <summary>
        /// 设置数据库表名
        /// </summary>
        /// <param name="dBname"></param>
        private static string SetDBName(DBname dBname)
        {
            switch (dBname)
            {
                case DBname.ACTION:
                    return TableName = "actionList";
                case DBname.CHILDACTION:
                    return TableName = "childList";
                case DBname.EVENT:
                    return TableName = "eventList";
                default:
                    return null;
            }                
        }

        private static List<string> _columnName = null;
        /// <summary>
        /// 字段名列表
        /// </summary>
        public static List<string> ColumnName
        {
            get { return _columnName; }
            set { _columnName = value; }
        }

        /// <summary>
        /// 设置字段名，返回字段名
        /// </summary>
        /// <param name="dBname"></param>
        /// <returns></returns>
        private static string[] SetColumnName(DBname dBname)
        {
            List<string> col = null;
            switch (dBname)
            {
                case DBname.ACTION:
                    col.AddRange(new string[2] { "aid", "actionName" });
                    break;
                case DBname.CHILDACTION:
                    col.AddRange(new string[4] { "cid", "childAction", "parentAction", "listOrder" });
                    break;
                case DBname.EVENT:
                    col.AddRange(new string[4] { "id", "eventName", "duration", "actionid"});
                    break;
                default:
                    break;
            }
            ColumnName = col;
            return col.ToArray();
        }
        /// <summary>
        /// 方法列表
        /// </summary>
        public enum DOING
        {
            SELECT,INSERT,UPDATE,DELETE
        }
        /// <summary>
        /// 设置前后SQL关键词命令组
        /// </summary>
        /// <param name="does">操作 增删改查</param>
        static void SetPreEndCommand(DOING does)
        {
            switch (does)
            {
                case DOING.SELECT:
                    PreCommand = "select * from ";
                    EndCommand = "where ";
                    break;
                case DOING.INSERT:
                    PreCommand = "insert into ";
                    EndCommand = "";
                    break;
                case DOING.UPDATE:
                    PreCommand = "update from ";
                    EndCommand = "where ";
                    break;
                case DOING.DELETE:
                    PreCommand = "delete from ";
                    EndCommand = "where ";
                    break;
                default:
                    break;
            }
        }
        #endregion
        /// <summary>
        /// 初始化
        /// </summary>
        private static void Init()
        {
            connection = new SQLiteConnection(SqlLiteHelper.GetConnectionString());
            connection.Open();

        }

        /// <summary>
        /// 释放连接
        /// </summary>
        private static void Dispose()
        {
            connection.Close();
            connection.Dispose();
        }

    }
}
