﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MediaPlayer
{
    class Debug:Form
    {
        
        public Debug()
        {

        }
        /// <summary>
        /// 显示提示
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="omsg">提示信息</param>
        /// <param name="isDebug">默认提示开</param>
        public void Show(object omsg =null,bool isDebug=true)
        {
            string msg = omsg.ToString();
            if (isDebug.ToString()==null)
            {
                isDebug = true;
            }
            if (isDebug)
            {
                ToolTip toolTip = new ToolTip();
                toolTip.Show("Debug: " + msg + "\r\n", this);
                //textBox.Text += "Debug: " + msg + "\r\n";
                //textBox.SelectionStart = textBox.Text.Length;//定位光标
                //textBox.ScrollToCaret();//滚动到光标处
            }
            return;
        }
    }
}
