﻿namespace MediaPlayer
{
    partial class ScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.b_AddTask = new System.Windows.Forms.Button();
            this.b_DelTask = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.b_AddAction = new System.Windows.Forms.Button();
            this.b_DelAction = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TaskdataGridView = new System.Windows.Forms.DataGridView();
            this.EventStatus = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Event = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Action = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiontreeView = new System.Windows.Forms.TreeView();
            this.cms_Action = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ActionAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StopTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseUnpauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NextTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PrivouseTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WaitPlayNtrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setVolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.muteVolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DelayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RunAppToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShutdownPlayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShutdownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cms_ChildAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.EditChildActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DelChildActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MoveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.TaskdataGridView)).BeginInit();
            this.cms_Action.SuspendLayout();
            this.cms_ChildAction.SuspendLayout();
            this.SuspendLayout();
            // 
            // b_AddTask
            // 
            this.b_AddTask.Location = new System.Drawing.Point(566, 18);
            this.b_AddTask.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.b_AddTask.Name = "b_AddTask";
            this.b_AddTask.Size = new System.Drawing.Size(64, 34);
            this.b_AddTask.TabIndex = 1;
            this.b_AddTask.Text = "添加";
            this.b_AddTask.UseVisualStyleBackColor = true;
            this.b_AddTask.Click += new System.EventHandler(this.B_AddTask_Click);
            // 
            // b_DelTask
            // 
            this.b_DelTask.Location = new System.Drawing.Point(639, 18);
            this.b_DelTask.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.b_DelTask.Name = "b_DelTask";
            this.b_DelTask.Size = new System.Drawing.Size(68, 34);
            this.b_DelTask.TabIndex = 2;
            this.b_DelTask.Text = "删除";
            this.b_DelTask.UseVisualStyleBackColor = true;
            this.b_DelTask.Click += new System.EventHandler(this.B_DelTask_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "计划列表 （右键管理单项）";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(20, 296);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "动作列表 （右键管理单个动作）";
            // 
            // b_AddAction
            // 
            this.b_AddAction.Location = new System.Drawing.Point(572, 286);
            this.b_AddAction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.b_AddAction.Name = "b_AddAction";
            this.b_AddAction.Size = new System.Drawing.Size(69, 34);
            this.b_AddAction.TabIndex = 6;
            this.b_AddAction.Text = "增加";
            this.b_AddAction.UseVisualStyleBackColor = true;
            this.b_AddAction.Click += new System.EventHandler(this.B_AddAction_Click);
            // 
            // b_DelAction
            // 
            this.b_DelAction.Location = new System.Drawing.Point(650, 286);
            this.b_DelAction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.b_DelAction.Name = "b_DelAction";
            this.b_DelAction.Size = new System.Drawing.Size(69, 34);
            this.b_DelAction.TabIndex = 7;
            this.b_DelAction.Text = "删除";
            this.b_DelAction.UseVisualStyleBackColor = true;
            this.b_DelAction.Click += new System.EventHandler(this.B_DelAction_Click);
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(296, 309);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(258, 3);
            this.label3.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Cursor = System.Windows.Forms.Cursors.No;
            this.label5.Location = new System.Drawing.Point(246, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(310, 3);
            this.label5.TabIndex = 9;
            // 
            // TaskdataGridView
            // 
            this.TaskdataGridView.AllowUserToAddRows = false;
            this.TaskdataGridView.AllowUserToDeleteRows = false;
            this.TaskdataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.TaskdataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TaskdataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TaskdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.TaskdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TaskdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventStatus,
            this.Event,
            this.Action});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TaskdataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.TaskdataGridView.GridColor = System.Drawing.SystemColors.Menu;
            this.TaskdataGridView.Location = new System.Drawing.Point(24, 56);
            this.TaskdataGridView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TaskdataGridView.MultiSelect = false;
            this.TaskdataGridView.Name = "TaskdataGridView";
            this.TaskdataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TaskdataGridView.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.TaskdataGridView.RowTemplate.Height = 23;
            this.TaskdataGridView.Size = new System.Drawing.Size(694, 225);
            this.TaskdataGridView.TabIndex = 10;
            // 
            // EventStatus
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.NullValue = false;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EventStatus.DefaultCellStyle = dataGridViewCellStyle6;
            this.EventStatus.FalseValue = "-1";
            this.EventStatus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.EventStatus.HeaderText = "";
            this.EventStatus.IndeterminateValue = "0";
            this.EventStatus.Name = "EventStatus";
            this.EventStatus.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.EventStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EventStatus.TrueValue = "1";
            this.EventStatus.Width = 20;
            // 
            // Event
            // 
            this.Event.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Event.HeaderText = "事件";
            this.Event.Name = "Event";
            // 
            // Action
            // 
            this.Action.HeaderText = "动作";
            this.Action.Name = "Action";
            // 
            // ActiontreeView
            // 
            this.ActiontreeView.Location = new System.Drawing.Point(24, 326);
            this.ActiontreeView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ActiontreeView.Name = "ActiontreeView";
            this.ActiontreeView.ShowLines = false;
            this.ActiontreeView.Size = new System.Drawing.Size(692, 244);
            this.ActiontreeView.TabIndex = 11;
            this.ActiontreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ActiontreeView_NodeMouseClick);
            // 
            // cms_Action
            // 
            this.cms_Action.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cms_Action.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ActionAddToolStripMenuItem,
            this.EditNameToolStripMenuItem,
            this.RemoveActionToolStripMenuItem});
            this.cms_Action.Name = "contextMenuStrip1";
            this.cms_Action.Size = new System.Drawing.Size(171, 88);
            // 
            // ActionAddToolStripMenuItem
            // 
            this.ActionAddToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlayTrackToolStripMenuItem,
            this.StopTrackToolStripMenuItem,
            this.pauseUnpauseToolStripMenuItem,
            this.NextTrackToolStripMenuItem,
            this.PrivouseTrackToolStripMenuItem,
            this.WaitPlayNtrackToolStripMenuItem,
            this.setVolToolStripMenuItem,
            this.muteVolToolStripMenuItem,
            this.PlayOrderToolStripMenuItem,
            this.DelayToolStripMenuItem,
            this.RunAppToolStripMenuItem,
            this.ShutdownPlayerToolStripMenuItem,
            this.ShutdownToolStripMenuItem});
            this.ActionAddToolStripMenuItem.Name = "ActionAddToolStripMenuItem";
            this.ActionAddToolStripMenuItem.Size = new System.Drawing.Size(170, 28);
            this.ActionAddToolStripMenuItem.Text = "添加动作";
            // 
            // PlayTrackToolStripMenuItem
            // 
            this.PlayTrackToolStripMenuItem.Name = "PlayTrackToolStripMenuItem";
            this.PlayTrackToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.PlayTrackToolStripMenuItem.Text = "开始播放";
            this.PlayTrackToolStripMenuItem.Click += new System.EventHandler(this.PlayTrackToolStripMenuItem_Click);
            // 
            // StopTrackToolStripMenuItem
            // 
            this.StopTrackToolStripMenuItem.Name = "StopTrackToolStripMenuItem";
            this.StopTrackToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.StopTrackToolStripMenuItem.Text = "停止播放";
            this.StopTrackToolStripMenuItem.Click += new System.EventHandler(this.StopTrackToolStripMenuItem_Click);
            // 
            // pauseUnpauseToolStripMenuItem
            // 
            this.pauseUnpauseToolStripMenuItem.Name = "pauseUnpauseToolStripMenuItem";
            this.pauseUnpauseToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.pauseUnpauseToolStripMenuItem.Text = "播放/停止 切换";
            this.pauseUnpauseToolStripMenuItem.Click += new System.EventHandler(this.PauseUnpauseToolStripMenuItem_Click);
            // 
            // NextTrackToolStripMenuItem
            // 
            this.NextTrackToolStripMenuItem.Name = "NextTrackToolStripMenuItem";
            this.NextTrackToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.NextTrackToolStripMenuItem.Text = "下一曲";
            this.NextTrackToolStripMenuItem.Click += new System.EventHandler(this.NextTrackToolStripMenuItem_Click);
            // 
            // PrivouseTrackToolStripMenuItem
            // 
            this.PrivouseTrackToolStripMenuItem.Name = "PrivouseTrackToolStripMenuItem";
            this.PrivouseTrackToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.PrivouseTrackToolStripMenuItem.Text = "上一曲";
            this.PrivouseTrackToolStripMenuItem.Click += new System.EventHandler(this.PrivouseTrackToolStripMenuItem_Click);
            // 
            // WaitPlayNtrackToolStripMenuItem
            // 
            this.WaitPlayNtrackToolStripMenuItem.Name = "WaitPlayNtrackToolStripMenuItem";
            this.WaitPlayNtrackToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.WaitPlayNtrackToolStripMenuItem.Text = "连续播放N首";
            this.WaitPlayNtrackToolStripMenuItem.Click += new System.EventHandler(this.WaitPlayNtrackToolStripMenuItem_Click);
            // 
            // setVolToolStripMenuItem
            // 
            this.setVolToolStripMenuItem.Name = "setVolToolStripMenuItem";
            this.setVolToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.setVolToolStripMenuItem.Text = "设置音量";
            this.setVolToolStripMenuItem.Click += new System.EventHandler(this.SetVolToolStripMenuItem_Click);
            // 
            // muteVolToolStripMenuItem
            // 
            this.muteVolToolStripMenuItem.Name = "muteVolToolStripMenuItem";
            this.muteVolToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.muteVolToolStripMenuItem.Text = "静音/恢复 切换";
            this.muteVolToolStripMenuItem.Click += new System.EventHandler(this.MuteVolToolStripMenuItem_Click);
            // 
            // PlayOrderToolStripMenuItem
            // 
            this.PlayOrderToolStripMenuItem.Name = "PlayOrderToolStripMenuItem";
            this.PlayOrderToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.PlayOrderToolStripMenuItem.Text = "播放顺序";
            this.PlayOrderToolStripMenuItem.Click += new System.EventHandler(this.PlayOrderToolStripMenuItem_Click);
            // 
            // DelayToolStripMenuItem
            // 
            this.DelayToolStripMenuItem.Name = "DelayToolStripMenuItem";
            this.DelayToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.DelayToolStripMenuItem.Text = "等待时间";
            this.DelayToolStripMenuItem.Click += new System.EventHandler(this.DelayToolStripMenuItem_Click);
            // 
            // RunAppToolStripMenuItem
            // 
            this.RunAppToolStripMenuItem.Name = "RunAppToolStripMenuItem";
            this.RunAppToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.RunAppToolStripMenuItem.Text = "执行命令行";
            this.RunAppToolStripMenuItem.Click += new System.EventHandler(this.RunAppToolStripMenuItem_Click);
            // 
            // ShutdownPlayerToolStripMenuItem
            // 
            this.ShutdownPlayerToolStripMenuItem.Name = "ShutdownPlayerToolStripMenuItem";
            this.ShutdownPlayerToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.ShutdownPlayerToolStripMenuItem.Text = "关闭播放器";
            this.ShutdownPlayerToolStripMenuItem.Click += new System.EventHandler(this.ShutdownPlayerToolStripMenuItem_Click);
            // 
            // ShutdownToolStripMenuItem
            // 
            this.ShutdownToolStripMenuItem.Name = "ShutdownToolStripMenuItem";
            this.ShutdownToolStripMenuItem.Size = new System.Drawing.Size(213, 30);
            this.ShutdownToolStripMenuItem.Text = "关机";
            this.ShutdownToolStripMenuItem.Click += new System.EventHandler(this.ShutdownToolStripMenuItem_Click);
            // 
            // EditNameToolStripMenuItem
            // 
            this.EditNameToolStripMenuItem.Name = "EditNameToolStripMenuItem";
            this.EditNameToolStripMenuItem.Size = new System.Drawing.Size(170, 28);
            this.EditNameToolStripMenuItem.Text = "编辑名称";
            this.EditNameToolStripMenuItem.Click += new System.EventHandler(this.EditNameToolStripMenuItem_Click);
            // 
            // RemoveActionToolStripMenuItem
            // 
            this.RemoveActionToolStripMenuItem.Name = "RemoveActionToolStripMenuItem";
            this.RemoveActionToolStripMenuItem.Size = new System.Drawing.Size(170, 28);
            this.RemoveActionToolStripMenuItem.Text = "删除动作组";
            this.RemoveActionToolStripMenuItem.Click += new System.EventHandler(this.RemoveActionToolStripMenuItem_Click);
            // 
            // cms_ChildAction
            // 
            this.cms_ChildAction.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cms_ChildAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EditChildActionToolStripMenuItem,
            this.DelChildActionToolStripMenuItem,
            this.toolStripSeparator1,
            this.MoveUpToolStripMenuItem,
            this.MoveDownToolStripMenuItem});
            this.cms_ChildAction.Name = "cms_ChildAction";
            this.cms_ChildAction.Size = new System.Drawing.Size(153, 122);
            // 
            // EditChildActionToolStripMenuItem
            // 
            this.EditChildActionToolStripMenuItem.Name = "EditChildActionToolStripMenuItem";
            this.EditChildActionToolStripMenuItem.Size = new System.Drawing.Size(152, 28);
            this.EditChildActionToolStripMenuItem.Text = "编辑";
            // 
            // DelChildActionToolStripMenuItem
            // 
            this.DelChildActionToolStripMenuItem.Name = "DelChildActionToolStripMenuItem";
            this.DelChildActionToolStripMenuItem.Size = new System.Drawing.Size(152, 28);
            this.DelChildActionToolStripMenuItem.Text = "删除";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // MoveUpToolStripMenuItem
            // 
            this.MoveUpToolStripMenuItem.Name = "MoveUpToolStripMenuItem";
            this.MoveUpToolStripMenuItem.Size = new System.Drawing.Size(152, 28);
            this.MoveUpToolStripMenuItem.Text = "往上移动";
            // 
            // MoveDownToolStripMenuItem
            // 
            this.MoveDownToolStripMenuItem.Name = "MoveDownToolStripMenuItem";
            this.MoveDownToolStripMenuItem.Size = new System.Drawing.Size(152, 28);
            this.MoveDownToolStripMenuItem.Text = "往下移动";
            // 
            // ScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 582);
            this.Controls.Add(this.ActiontreeView);
            this.Controls.Add(this.TaskdataGridView);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.b_DelAction);
            this.Controls.Add(this.b_AddAction);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_DelTask);
            this.Controls.Add(this.b_AddTask);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ScheduleForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "定时计划";
            ((System.ComponentModel.ISupportInitialize)(this.TaskdataGridView)).EndInit();
            this.cms_Action.ResumeLayout(false);
            this.cms_ChildAction.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button b_AddTask;
        private System.Windows.Forms.Button b_DelTask;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button b_AddAction;
        private System.Windows.Forms.Button b_DelAction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView TaskdataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EventStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Event;
        private System.Windows.Forms.DataGridViewTextBoxColumn Action;
        private System.Windows.Forms.TreeView ActiontreeView;
        private System.Windows.Forms.ContextMenuStrip cms_Action;
        private System.Windows.Forms.ToolStripMenuItem ActionAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemoveActionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PlayTrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StopTrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseUnpauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NextTrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PrivouseTrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WaitPlayNtrackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setVolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem muteVolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PlayOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DelayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RunAppToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShutdownPlayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShutdownToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cms_ChildAction;
        private System.Windows.Forms.ToolStripMenuItem EditChildActionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DelChildActionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MoveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MoveDownToolStripMenuItem;
    }
}