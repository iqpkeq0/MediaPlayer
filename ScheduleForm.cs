﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static MediaPlayer.SqlLiteHelper;

namespace MediaPlayer
{
    public partial class ScheduleForm : Form
    {
        public ScheduleForm()
        {
            InitializeComponent();
            SelectAction();
        }

        private void B_AddTask_Click(object sender, EventArgs e)
        {
            
            TaskForm taskForm = new TaskForm(this);
            taskForm.Show();
            
        }

        private void B_DelTask_Click(object sender, EventArgs e)
        {
            int selectStatusIndex = TaskdataGridView.CurrentRow.Index;
            if (selectStatusIndex != -1) { 
            TaskdataGridView.Rows.RemoveAt(selectStatusIndex);
            }
        }

        public void AddTask(string str)
        {
            object item = "123";
            item = str;
            TaskdataGridView.Rows.Add(false,item, "");
        }

        private void B_AddAction_Click(object sender, EventArgs e)
        {
            ActiontreeView.SelectedNode = null;//如果是添加，去除焦点选中
            ActionForm actionForm = new ActionForm(this);
            actionForm.Show();

        }

        private void B_DelAction_Click(object sender, EventArgs e)           
        {
            int selectStatusIndex = ActiontreeView.Nodes.IndexOf(ActiontreeView.SelectedNode);
            if(selectStatusIndex != -1) {
                DeleteAction();
                
            }
        }
        

        public void ActiontreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point point = new Point(e.Node.Bounds.X+e.Node.Bounds.Width, e.Node.Bounds.Y + e.Node.Bounds.Height / 2);
                ActiontreeView.SelectedNode = e.Node;
                
                if(ActiontreeView.SelectedNode.Parent != null)//选中节点为子节点
                {
                    this.cms_ChildAction.Show(this.ActiontreeView, point);
                }
                else
                {
                    this.cms_Action.Show(this.ActiontreeView, point);//父节点展示父节点菜单
                }

            }
        }


        /// <summary>
        /// 添加动作组
        /// </summary>
        /// <param name="Name">动作组名</param>
        /// <returns>返回bool,是否添加成功</returns>
        public bool AddAction(string Name)
        {
            bool hasname = CheckDuplicateActionName(Name);
            if (!hasname)
            {
                ActiontreeView.LabelEdit = true;//使其处于可编辑状态
                TreeNode node = new TreeNode
                {
                    Text = Name
                };//初始化一个节点
                ActiontreeView.Nodes.Add(node);//添加根节点
                SqlLiteHelper.InsertAction(Name);
                //TODO:Testing----------------SQL插入数据，测试通用SQL入口;
                //SqlLiteHelper.ExecuteSQL(DOING.INSERT, DBname.ACTION,);
                return true;
            }
            else
            {
                return false;
            }
            


        }
        /// <summary>
        /// 编辑动作组名
        /// </summary>
        /// <param name="str">新数据</param>
        /// <param name="oldName">旧数据</param>
        public bool EditAction(string str="",string oldName="")
        {

            bool hasname=CheckDuplicateActionName(str);
            if (!hasname)
            {
                ActiontreeView.SelectedNode.BeginEdit();
                string OldName = ActiontreeView.SelectedNode.Text;
                ActiontreeView.SelectedNode.Text = str;
                ActiontreeView.SelectedNode.EndEdit(true);
                SqlLiteHelper.UpdateAction(str, OldName);
                return true;
            }else { return false; }
            
        }
        /// <summary>
        /// 查询动作组名
        /// </summary>
        public void SelectAction()
        {
            List<string> result = SqlLiteHelper.SelectAction();
            ActiontreeView.LabelEdit = true;//使其处于可编辑状态           
            if(result != null) { 
                foreach(string n in result)
                {
                    TreeNode node = new TreeNode();//初始化一个节点
                    node.Text = n;
                    ActiontreeView.Nodes.Add(node);//添加根节点
                }
            }

        }
        /// <summary>
        /// 删除动作组名
        /// </summary>
        public void DeleteAction()
        { 
            if (GetActionNodeStatusChecked())
            {
                TreeNode treeNode = ActiontreeView.SelectedNode;
                string delName = treeNode.Text;
                SqlLiteHelper.DeleteAction(delName);
                ActiontreeView.Nodes.Remove(ActiontreeView.SelectedNode);
            }

        }

        /// <summary>
        /// 检查动作组是否有重复
        /// </summary>
        /// <param name="checkName">动作组名</param>
        /// <returns>返回是否有重复 默认是会重复</returns>
        public bool CheckDuplicateActionName(string checkName="")
        {
            bool re = false;
            int ExceptIndex = -1;
            if (ActiontreeView.SelectedNode != null)//如果没有选中的状态
            {
                ExceptIndex = ActiontreeView.SelectedNode.Index;
            }

            for (int i = 0; i < ActiontreeView.Nodes.Count; i++)
            {
                if (ActiontreeView.Nodes[i].Text == checkName)
                {
                    if (i != ExceptIndex) { return re = true; }
                }
            }       
            return re;
        }

        /// <summary>
        /// 返回节点选中状态;选中状态true
        /// </summary>
        public bool GetActionNodeStatusChecked()
        {
            TreeNode treeNode= ActiontreeView.SelectedNode;
            if (treeNode == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 添加单个动作
        /// </summary>
        /// <returns></returns>
        public void AddChildAction(string str="")
        {
            TreeNode treeNode = ActiontreeView.SelectedNode;
            TreeNode ChildNode = new TreeNode
            {
                Text = str
            };
            if (treeNode != null)
            {
                treeNode.Nodes.Add(ChildNode);
            }
            treeNode.ExpandAll();

        }



        private void EditNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionForm actionForm = new ActionForm(this);
            ActiontreeView.SelectedNode.BeginEdit();
            actionForm.SetActionName(ActiontreeView.SelectedNode.Text);
            actionForm.Show();
            
        }
        private void RemoveActionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GetActionNodeStatusChecked()) {
                DeleteAction();
            }
        }


        private void PlayTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string str = "开始播放";
            AddChildAction(str);


        }

        private void StopTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void PauseUnpauseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void NextTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void PrivouseTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void WaitPlayNtrackToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void SetVolToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MuteVolToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void PlayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void DelayToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void RunAppToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ShutdownPlayerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ShutdownToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
