﻿namespace MediaPlayer
{
    partial class TaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_TaskName = new System.Windows.Forms.TextBox();
            this.DurationComboBox = new System.Windows.Forms.ComboBox();
            this.timeChooseEdit = new DevExpress.XtraEditors.TimeEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.b_saveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.timeChooseEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_TaskName
            // 
            this.tb_TaskName.Location = new System.Drawing.Point(71, 29);
            this.tb_TaskName.Name = "tb_TaskName";
            this.tb_TaskName.Size = new System.Drawing.Size(100, 21);
            this.tb_TaskName.TabIndex = 0;
            // 
            // DurationComboBox
            // 
            this.DurationComboBox.FormattingEnabled = true;
            this.DurationComboBox.Location = new System.Drawing.Point(71, 71);
            this.DurationComboBox.Name = "DurationComboBox";
            this.DurationComboBox.Size = new System.Drawing.Size(121, 20);
            this.DurationComboBox.TabIndex = 1;
            // 
            // timeChooseEdit
            // 
            this.timeChooseEdit.EditValue = new System.DateTime(2018, 1, 16, 0, 0, 0, 0);
            this.timeChooseEdit.Location = new System.Drawing.Point(71, 113);
            this.timeChooseEdit.Name = "timeChooseEdit";
            this.timeChooseEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeChooseEdit.Size = new System.Drawing.Size(100, 20);
            this.timeChooseEdit.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "任务名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "周期";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "选择时间";
            // 
            // b_saveButton
            // 
            this.b_saveButton.Location = new System.Drawing.Point(96, 171);
            this.b_saveButton.Name = "b_saveButton";
            this.b_saveButton.Size = new System.Drawing.Size(75, 23);
            this.b_saveButton.TabIndex = 6;
            this.b_saveButton.Text = "保存";
            this.b_saveButton.Click += new System.EventHandler(B_saveButton_Click);
            this.b_saveButton.UseVisualStyleBackColor = true;
            // 
            // TaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.b_saveButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.timeChooseEdit);
            this.Controls.Add(this.DurationComboBox);
            this.Controls.Add(this.tb_TaskName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaskForm";
            this.ShowInTaskbar = false;
            this.Text = "任务列表";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.timeChooseEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_TaskName;
        private System.Windows.Forms.ComboBox DurationComboBox;
        private DevExpress.XtraEditors.TimeEdit timeChooseEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_saveButton;
    }
}