﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MediaPlayer
{
    public class MJson
    {
        /// <summary>
        /// 默认初始化方法
        /// </summary>
        public MJson()
        {

        }



        /// <summary>
        /// 读取属性值
        /// </summary>
        /// <param name="attr">所需属性</param>
        /// <returns></returns>
        public string  Read(string attr) {
            string attrValue;
            string FilePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;//初始目录为程序执行目录;
            using (StreamReader reader = File.OpenText(FilePath + @"dll\config.json"))
            {
                JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                attrValue = o[attr].ToString();
            }
            return attrValue;
        }
        /// <summary>
        /// 改属性值
        /// </summary>
        /// <param name="attr">属性名</param>
        /// <param name="value">属性值</param>
        /// <returns></returns>
        public bool Edit(string attr,object value)
        {
            string oldValue;
            string FilePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;//初始目录为程序执行目录;
            try
            {
                StreamReader reader = File.OpenText(FilePath + @"dll\config.json");               
                JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                reader.Close();
                oldValue = o[attr].ToString();
                o[attr] = value.ToString();
                if (o[attr].ToString() != oldValue)
                {
                    File.WriteAllText((FilePath + @"dll\config.json"), FormatJsonStr(o.ToString()));
                    return true;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            //与旧值相同，不修改，或修改失败。
            return false;
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        /// <summary>
        /// 格式化JSON字符串
        /// </summary>
        /// <param name="str">输入字符串</param>
        /// <returns>输出字符串</returns>
        public static string FormatJsonStr(string str)
        {
            JsonSerializer serializer = new JsonSerializer();
            TextReader tr = new StringReader(str);
            JsonTextReader jtr = new JsonTextReader(tr);
            object obj = serializer.Deserialize(jtr);
            if (obj != null)
            {
                StringWriter textWriter = new StringWriter();
                JsonTextWriter jsonWriter = new JsonTextWriter(textWriter)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4,
                    IndentChar = ' '
                };
                serializer.Serialize(jsonWriter, obj);
                return textWriter.ToString();
            }
            else
            {
                return str;
            }
        }
    }
}
