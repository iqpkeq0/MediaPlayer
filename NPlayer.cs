﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaPlayer
{
    class NPlayer
    {
        public NPlayer()
        {

        }
        private IWavePlayer wavePlayer;
        private AudioFileReader audioFileReader;
        /// <summary>
        /// 播放文件路径
        /// </summary>
       public string FileName = "";
        private bool isPlaying = false;
        /// <summary>
        /// 是否正在播放
        /// </summary>
        public bool IsPlaying
        {
            get { return isPlaying; }
        }

        /// <summary>
        /// 当前时间
        /// </summary>
        public TimeSpan CurrentTime
        {
            get
            {
                if (audioFileReader == null)
                {
                    return TimeSpan.Zero;
                }
                else
                {
                    return audioFileReader.CurrentTime;
                }
            }
        }
        /// <summary>
        /// 总时间
        /// </summary>
        public TimeSpan TotalTime
        {
            get
            {
                if (audioFileReader == null)
                {
                    return TimeSpan.Zero;
                }
                else
                {
                    return audioFileReader.TotalTime;
                }
            }
        }

        private float volume = 1f;
        /// <summary>
        /// 音量
        /// </summary>
        public float Volume
        {
            get { return volume; }
            set {
                if (value>=0 && value <= 1f)
                {
                    volume = value;
                    if (audioFileReader != null)
                    {
                        audioFileReader.Volume = value;
                    }
                }
             }
        }
        /// <summary>
        /// 播放
        /// </summary>
        public void Play()
        {
            if (string.IsNullOrEmpty(FileName)) { return; }
            if (isPlaying) { wavePlayer.Stop();wavePlayer.Dispose();audioFileReader.Dispose(); }
            wavePlayer = new WaveOut();
            audioFileReader = new AudioFileReader(FileName);
            audioFileReader.Volume = volume;
            wavePlayer.Init(audioFileReader);
            wavePlayer.PlaybackStopped += OnPlaybackStopped;
            wavePlayer.Play();
            isPlaying = true;
                
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            if(audioFileReader != null)
            {
                audioFileReader.Dispose();
                audioFileReader = null;
            }
            if (wavePlayer != null)
            {
                wavePlayer.Dispose();
                wavePlayer = null;
            }
            isPlaying = false;
        }
        /// <summary>
        /// 停止
        /// </summary>
        public void Stop()
        {
            if(wavePlayer != null)
            {
                wavePlayer.Stop();
            }
        }

    }
}
