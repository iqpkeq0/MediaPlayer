﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MediaPlayer
{
    public partial class ActionForm : Form
    {
        ScheduleForm scheduleForm = null;//将本窗体的拥有者强制设为父窗口的实例
        public ActionForm(ScheduleForm F1)
        {
            
            InitializeComponent();
            this.scheduleForm = F1;//返回父窗口
            if (tb_ActonName.Text == null) { //为空
                SetActionName("动作");
            }
            
        }

        /// <summary>
        /// 设置动作组名
        /// </summary>
        /// <param name="str">动作组名</param>
        public void SetActionName(string str="")
        {
            tb_ActonName.Text = str;
            
        }
        /// <summary>
        /// 获取动作组名
        /// </summary>
        /// <returns></returns>
        public string  GetActionName()
        {
            return tb_ActonName.Text;
        }

        private void ActionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.scheduleForm.Visible = true;//恢复父窗口
        }

        public new void Close()
        {
            this.Dispose();
        }

        private void B_Save_Click(object sender, EventArgs e)
        {
            //TODO: 添加同名
            string msg = tb_ActonName.Text;
            
            bool ifSucess = false;
            if(!scheduleForm.GetActionNodeStatusChecked()) {
                  ifSucess=scheduleForm.AddAction(msg);
                }
            else
                {
                ifSucess=scheduleForm.EditAction(msg);                    
            }
            this.Visible = !ifSucess;
            if (!ifSucess)
            {
                duplicateToolTip.Show("已有动作重复", tb_ActonName);
            }
            




        }
    }
}
