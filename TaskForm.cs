﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MediaPlayer
{
    public partial class TaskForm : Form
    {
        ScheduleForm scheduleForm = null;//将本窗体的拥有者强制设为父窗口的实例
        public TaskForm(ScheduleForm F1)
        {
            InitializeComponent();
            this.scheduleForm = F1;//返回父窗口
            SetInitTaskForm();

        }

        private void TaskForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.scheduleForm.Visible = true;//恢复父窗口
        }
        /// <summary>
        /// 初始化表单数值
        /// </summary>
        private void SetInitTaskForm()
        {
            tb_TaskName.Text = "任务" + 1;
            string[] items = { "每日重复","每周重复","每月重复","每年重复"};
            DurationComboBox.Items.AddRange(items);
            DurationComboBox.SelectedIndex = 1;//默认每天重复
            timeChooseEdit.Visible = true;//激活时间控件
        }
        private void B_saveButton_Click(object sender, EventArgs e)
        {
            string msg = tb_TaskName.Text + "(" + "每天重复" + ")" + timeChooseEdit.Text;
            scheduleForm.AddTask(msg);
            this.Visible = false;

        }
        public new void Close()
        {
            this.Dispose();
        }
    }
}
